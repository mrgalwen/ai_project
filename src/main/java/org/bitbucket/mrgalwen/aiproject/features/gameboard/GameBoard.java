package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
public class GameBoard {
    private final FieldMark[][] board;
    private final int size;
    private OnFieldChangedListener onFieldChangedListener;

    public GameBoard(int size) {
        if (size < 3) {
            throw new IllegalArgumentException("Size must be greater or equals 3!");
        }

        this.size = size;
        board = new FieldMark[size][size];

        for (FieldMark[] fieldMarks : board) {
            Arrays.fill(fieldMarks, FieldMark.EMPTY);
        }
    }

    public int size() {
        return size;
    }

    public FieldMark getFieldMark(int row, int column) {
        return getFieldMark(new Move(row, column));
    }

    public FieldMark getFieldMark(Move move) {
        return board[move.getRow()][move.getColumn()];
    }

    public void setFieldMark(int row, int column, FieldMark fieldMark) {
        setFieldMark(new Move(row, column), fieldMark);
    }

    public void setFieldMark(Move move, FieldMark fieldMark) {
        setFieldMarkQuietly(move, fieldMark);

        if (Objects.nonNull(onFieldChangedListener)) {
            onFieldChangedListener.onFieldChanged(move, fieldMark);
        }
    }

    public void setFieldMarkQuietly(int row, int column, FieldMark fieldMark) {
        setFieldMarkQuietly(new Move(row, column), fieldMark);
    }

    public void setFieldMarkQuietly(Move move, FieldMark fieldMark) {
        board[move.getRow()][move.getColumn()] = fieldMark;
    }

        public Optional<GameWinner> checkGameWinner() {
        boolean emptyFieldFound = false;

        // horizontally
        for (int row = 0; row < size(); row++) {
            FieldMark lookingFor = null;
            boolean winnerFound = false;

            for (int column = 0; column < size(); column++) {
                winnerFound = true;

                FieldMark fieldMark = getFieldMark(row, column);

                if (fieldMark == FieldMark.EMPTY) {
                    emptyFieldFound = true;
                    winnerFound = false;
                    break;
                }

                if (lookingFor == null) {
                    lookingFor = fieldMark;
                    continue;
                }

                if (fieldMark != lookingFor) {
                    winnerFound = false;
                    break;
                }
            }

            if (winnerFound) {
                return Optional.of(new GameWinner(lookingFor));
            }
        }

        // vertically
        for (int column = 0; column < size(); column++) {
            FieldMark lookingFor = null;
            boolean winnerFound = false;

            for (int row = 0; row < size(); row++) {
                winnerFound = true;

                FieldMark fieldMark = getFieldMark(row, column);

                if (fieldMark == FieldMark.EMPTY) {
                    emptyFieldFound = true;
                    winnerFound = false;
                    break;
                }

                if (lookingFor == null) {
                    lookingFor = fieldMark;
                    continue;
                }

                if (fieldMark != lookingFor) {
                    winnerFound = false;
                    break;
                }
            }

            if (winnerFound) {
                return Optional.of(new GameWinner(lookingFor));
            }
        }

        // diagonally
        Optional<GameWinner> gameWinner = checkWinnerDiagonally();

        if (gameWinner.isPresent()) {
            return gameWinner;
        }

        if (!emptyFieldFound) {
            return Optional.of(new GameWinner(true));
        }

        return Optional.empty();
    }

    private Optional<GameWinner> checkWinnerDiagonally() {
        FieldMark lookingFor = null;
        boolean winnerFound = true;

        for (int i = 0; i < size(); i++) {
            FieldMark fieldMark = getFieldMark(i, i);

            if (fieldMark == FieldMark.EMPTY) {
                winnerFound = false;
                break;
            }

            if (lookingFor == null) {
                lookingFor = fieldMark;
                continue;
            }

            if (fieldMark != lookingFor) {
                winnerFound = false;
                break;
            }
        }

        if (winnerFound) {
            return Optional.of(new GameWinner(lookingFor));
        }

        lookingFor = null;
        winnerFound = true;

        for (int i = 0; i < size(); i++) {
            FieldMark fieldMark = getFieldMark(i, size() - i - 1);

            if (fieldMark == FieldMark.EMPTY) {
                winnerFound = false;
                break;
            }

            if (lookingFor == null) {
                lookingFor = fieldMark;
                continue;
            }

            if (fieldMark != lookingFor) {
                winnerFound = false;
                break;
            }
        }

        if (winnerFound) {
            return Optional.of(new GameWinner(lookingFor));
        }

        return Optional.empty();
    }

    public void setOnFieldChangedListener(OnFieldChangedListener onFieldChangedListener) {
        this.onFieldChangedListener = onFieldChangedListener;
    }
}

