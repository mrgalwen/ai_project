package org.bitbucket.mrgalwen.aiproject.features.gameboard.ai;

import javafx.concurrent.Task;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.FieldMark;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.GameBoard;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.GameWinner;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.Move;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
@Slf4j
public class Ai {
    private final FieldMark aiPlayerMark;
    private final FieldMark humanPlayerMark;
    private Random random = new Random();

    @Getter
    @Setter
    private AiListener listener;

    public interface AiListener {
        void onStart();
        void onFinish();
    }

    @Getter
    @Setter
    private int iq = 8;

    public Ai(FieldMark aiFieldMark) {
        this.aiPlayerMark = Objects.requireNonNull(aiFieldMark);

        if (aiPlayerMark == FieldMark.CIRCLE) {
            humanPlayerMark = FieldMark.CROSS;
        } else {
            humanPlayerMark = FieldMark.CIRCLE;
        }
    }

    public void performMove(GameBoard gameBoard) {
//        AiMove bestMove = calcBestMove2(gameBoard, aiPlayerMark, iq);
//        AiMove bestMove = calcBestMove(gameBoard, aiPlayerMark, iq, Integer.MIN_VALUE, Integer.MAX_VALUE);

        if (listener != null) {
            listener.onStart();;
        }

        Task<AiMove> task = new Task<AiMove>() {
            @Override protected AiMove call() throws Exception {
//                return calcBestMove2(gameBoard, aiPlayerMark, iq);
                return calcBestMove(gameBoard, aiPlayerMark, iq, Integer.MIN_VALUE, Integer.MAX_VALUE);
            }

            @Override
            protected void succeeded() {
                try {
                    AiMove bestMove = get();


                    log.info("{}", bestMove);

                    Optional<GameWinner> gameWinnerOptional = gameBoard.checkGameWinner();

                    if (gameWinnerOptional.isPresent()) {
                        GameWinner gameWinner = gameWinnerOptional.get();
                    } else {
                        gameBoard.setFieldMark(bestMove.getRow(), bestMove.getColumn(), aiPlayerMark);
                    }

                    if (listener != null) {
                        listener.onFinish();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        };

        Executors.newSingleThreadExecutor().submit(task);

//        log.info("{}", bestMove);
//
//        Optional<GameWinner> gameWinnerOptional = gameBoard.checkGameWinner();
//
//        if (gameWinnerOptional.isPresent()) {
//            GameWinner gameWinner = gameWinnerOptional.get();
//        } else {
//            gameBoard.setFieldMark(bestMove.getRow(), bestMove.getColumn(), aiPlayerMark);
//        }
    }

    private AiMove calcBestMove2(GameBoard gameBoard, FieldMark player, int depth) {
        // Generate possible next moves in a List of int[2] of {row, col}.
        List<Move> nextMoves = generateMoves(gameBoard);

        // aiPlayerMark is maximizing; while oppSeed is minimizing
        int bestScore = (player == aiPlayerMark) ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        int currentScore = 0;
        int bestRow = -1;
        int bestCol = -1;

        if (nextMoves.isEmpty() || depth == 0) {
            // Gameover or depth reached, evaluate score
            bestScore = evaluate(gameBoard);
        } else {
            for (Move move : nextMoves) {
                // Try this move for the current "player"

                gameBoard.setFieldMarkQuietly(move.getRow(), move.getColumn(), player);

                if (player == aiPlayerMark) {  // aiPlayerMark (computer) is maximizing player

                    currentScore = calcBestMove2(gameBoard, humanPlayerMark, depth - 1).getScore();

                    if (currentScore > bestScore) {
                        bestScore = currentScore;
                        bestRow = move.getRow();
                        bestCol = move.getColumn();
                    }
                } else {  // oppSeed is minimizing player
                    currentScore = calcBestMove2(gameBoard, aiPlayerMark, depth - 1).getScore();

                    if (currentScore < bestScore) {
                        bestScore = currentScore;
                        bestRow = move.getRow();
                        bestCol = move.getColumn();
                    }
                }
                // Undo move
                gameBoard.setFieldMarkQuietly(move.getRow(), move.getColumn(), FieldMark.EMPTY);
            }
        }

        return new AiMove(bestRow, bestCol, bestScore);
    }

    public AiMove calcBestMove(GameBoard gameBoard, FieldMark player, int depth, int alpha, int beta) {
        // Generate possible next moves in a list of int[2] of {row, col}.
        List<Move> nextMoves = generateMoves(gameBoard);

        // mySeed is maximizing; while oppSeed is minimizing
        int score = 0;
        int bestRow = -1;
        int bestCol = -1;

        if (nextMoves.isEmpty() || depth == 0) {
            // Gameover or depth reached, evaluate score
            score = evaluate(gameBoard);
            return new AiMove(bestRow, bestCol, score);
        } else {
            for (Move move : nextMoves) {
                // try this move for the current "player"

                gameBoard.setFieldMarkQuietly(move, player);

//                if (true) {
                if (random.nextBoolean()) {

                    if (player == aiPlayerMark) {  // mySeed (computer) is maximizing player
                        score = calcBestMove(gameBoard, humanPlayerMark, depth - 1, alpha, beta).getScore();

                        if (score > alpha) {
                            alpha = score;
                            bestRow = move.getRow();
                            bestCol = move.getColumn();
                        }
                    } else {  // oppSeed is minimizing player
                        score = calcBestMove(gameBoard, aiPlayerMark, depth - 1, alpha, beta).getScore();

                        if (score < beta) {
                            beta = score;
                            bestRow = move.getRow();
                            bestCol = move.getColumn();
                        }
                    }
                }
                // undo move
                gameBoard.setFieldMarkQuietly(move, FieldMark.EMPTY);

                // cut-off
                if (alpha >= beta) {
                    break;
                }
            }

            return new AiMove(bestRow, bestCol, (player == aiPlayerMark) ? alpha : beta);
        }
    }

    /**
     * Find all valid next moves.
     * Return List of moves in int[2] of {row, col} or empty list if gameover
     */
    private List<Move> generateMoves(GameBoard gameBoard) {
        List<Move> nextMoves = new ArrayList<>(); // allocate List

        // If gameover, i.e., no next move
        Optional<GameWinner> gameWinnerOptional = gameBoard.checkGameWinner();

        if (gameWinnerOptional.isPresent() && !gameWinnerOptional.get().isTie()) {
            return nextMoves;
        }

        // Search for empty cells and add to the List
        for (int row = 0; row < gameBoard.size(); ++row) {
            for (int col = 0; col < gameBoard.size(); ++col) {
                if (gameBoard.getFieldMark(row, col) == FieldMark.EMPTY) {
                    nextMoves.add(new Move(row, col));
                }
            }
        }

        return nextMoves;
    }

    private int evaluate(GameBoard gameBoard) {
        int score = 0;
        // Evaluate score for each of the 8 lines (3 rows, 3 columns, 2 diagonals)
        for (int i = 0; i < gameBoard.size(); i++) {
            score += evaluateColumn(gameBoard, i);
            score += evaluateRow(gameBoard, i);
        }

        score += evaluateDiagonal(gameBoard, false);  // diagonal
        score += evaluateDiagonal(gameBoard, true);  // diagonal
        return score;
    }

    private int evaluateRow(GameBoard gameBoard, int row) {
        List<Move> moves = new ArrayList<>();

        for (int i = 0; i < gameBoard.size(); i++) {
            moves.add(new Move(row, i));
        }

        return evaluateLine(gameBoard, moves);
    }

    private int evaluateColumn(GameBoard gameBoard, int column) {
        List<Move> moves = new ArrayList<>();

        for (int i = 0; i < gameBoard.size(); i++) {
            moves.add(new Move(i, column));
        }

        return evaluateLine(gameBoard, moves);
    }

    private int evaluateDiagonal(GameBoard gameBoard, boolean alternateDiagnoal) {
        List<Move> moves = new ArrayList<>();

        if (alternateDiagnoal) {
            for (int i = 0; i < gameBoard.size(); i++) {
                moves.add(new Move(i, gameBoard.size() - i - 1));
            }
        } else {
            for (int i = 0; i < gameBoard.size(); i++) {
                moves.add(new Move(i, i));
            }
        }

        return evaluateLine(gameBoard, moves);
    }

    private int evaluateLine(GameBoard gameBoard, List<Move> moves) {
        int score = 0;

        Move move = moves.get(0);

        // First cell
        if (gameBoard.getFieldMark(move) == aiPlayerMark) {
            score = 1;
        } else if (gameBoard.getFieldMark(move) == humanPlayerMark) {
            score = -1;
        }

        for (int i = 1; i < moves.size(); i++) {
            move = moves.get(i);
            if (gameBoard.getFieldMark(move) == aiPlayerMark) {
                if (score == 1) {   // cell1 is aiPlayerMark
                    score = i == moves.size() - 1 ? score * 10 : 10;
                } else if (score == -1) {  // cell1 is oppSeed
                    return 0;
                } else {  // cell1 is empty
                    score = 1;
                }
            } else if (gameBoard.getFieldMark(move) == humanPlayerMark) {
                if (score == -1) { // cell1 is oppSeed
                    score = i == moves.size() - 1 ? score * 10 : -10;
                } else if (score == 1) { // cell1 is aiPlayerMark
                    return 0;
                } else {  // cell1 is empty
                    score = -1;
                }
            }
        }

        return score;
    }
}
