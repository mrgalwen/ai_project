package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.i18n.I18n;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.view.View;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
public class GameBoardView extends View<GameBoardPresenter> {
    public GameBoardView() {
        super(I18n.createBinding(I18n.APP, "App.title"));
    }
}
