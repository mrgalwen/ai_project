package org.bitbucket.mrgalwen.aiproject.features.gameboard;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
public interface OnFieldChangedListener {
    void onFieldChanged(Move move, FieldMark fieldMark);
}
