package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
@Getter
@Setter
@ToString
public class Move {
    private int row;
    private int column;

    public Move(int row, int column) {
        setRow(row);
        setColumn(column);
    }
}
