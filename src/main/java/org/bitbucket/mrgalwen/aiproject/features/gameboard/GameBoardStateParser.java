package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Michał Kozioł
 * @since 15.12.2016
 */
public final class GameBoardStateParser {
    public static Pair<List<Pair<FieldMark, Move>>, FieldMark> parse(InputStream inputStream, int gameBoardSize) throws GameBoardStateParserException {
        try (
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader reader = new BufferedReader(inputStreamReader)
        ) {
            return parse(reader.lines(), gameBoardSize);
        } catch (Exception e) {
            throw new GameBoardStateParserException();
        }
    }

    public static Pair<List<Pair<FieldMark, Move>>, FieldMark> parse(Stream<String> stream, int gameBoardSize) throws GameBoardStateParserException {

        final int[] markCount = {-1};

        List<Pair<FieldMark, Move>> moves = new ArrayList<>();

        stream.forEachOrdered(s -> {
            String[] split = s.split(",");

            if (split.length == 1 && markCount[0] == -1) {
                markCount[0] = Integer.valueOf(split[0]);
            } else if (moves.size() < markCount[0] && split.length == 3) {
                FieldMark mark = Integer.valueOf(split[0]) == 0 ? FieldMark.CIRCLE : FieldMark.CROSS;
                Move move = new Move(Integer.valueOf(split[1]) - 1, Integer.valueOf(split[2]) - 1);
                moves.add(new Pair<>(mark, move));
            }
        });

        FieldMark next = null;

        if (!moves.isEmpty()) {
            if (moves.get(moves.size() - 1).getKey() == FieldMark.CIRCLE) {
                next = FieldMark.CROSS;
            } else {
                next = FieldMark.CIRCLE;
            }
        }

        return new Pair<>(moves, next);
    }
}
