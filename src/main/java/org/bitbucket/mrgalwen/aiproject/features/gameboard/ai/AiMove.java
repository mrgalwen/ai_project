package org.bitbucket.mrgalwen.aiproject.features.gameboard.ai;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.Move;

/**
 * @author Michał Koziol
 * @since 13.12.2016
 */
@Getter
@Setter
@ToString(callSuper = true)
public class AiMove extends Move {
    private int score;

    @Builder
    public AiMove(int row, int column, int score) {
        super(row, column);
        setScore(score);
    }
}
