package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import lombok.ToString;

/**
 * @author Michał Kozioł
 * @since 14.12.2016
 */
@ToString
public final class GameWinner {
    private FieldMark winnerFieldMark;
    private boolean tie;

    public GameWinner(FieldMark winnerFieldMark) {
        this.winnerFieldMark = winnerFieldMark;
    }

    public GameWinner(boolean tie) {
        this.tie = tie;
    }

    public FieldMark getWinnerFieldMark() {
        return winnerFieldMark;
    }

    public boolean isTie() {
        return tie;
    }
}
