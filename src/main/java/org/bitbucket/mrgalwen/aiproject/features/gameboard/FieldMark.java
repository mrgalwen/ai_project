package org.bitbucket.mrgalwen.aiproject.features.gameboard;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
public enum FieldMark {
    EMPTY,
    CROSS,
    CIRCLE
}
