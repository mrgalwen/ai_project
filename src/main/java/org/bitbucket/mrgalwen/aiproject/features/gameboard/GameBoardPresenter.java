package org.bitbucket.mrgalwen.aiproject.features.gameboard;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.utils.MaterialDesignIconFactory;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.ai.Ai;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.ai.AiMove;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.presenter.AbstractPresenter;
import org.bitbucket.mrgalwen.aiproject.utils.GridPaneUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
@Slf4j
@SuppressWarnings("unused")
public class GameBoardPresenter extends AbstractPresenter<GameBoardView> {
    private FieldMark humanPlayer = FieldMark.CIRCLE;
    private FieldMark aiPlayer = FieldMark.CROSS;

    private static final Integer BOARD_SIZE = 10;

    private Ai ai;

    @FXML
    protected GridPane boardGridPane;

    @FXML
    private ProgressBar aiProgress;

    private Node filler() {
        Region region = new Region();
        int size = 300 / gameBoardProperty().getValue().size();

        region.setMinHeight(size);
        region.setMinWidth(size);

        return region;
    }

    private ObjectProperty<GameBoard> gameBoard = new SimpleObjectProperty<GameBoard>() {
        @Override
        protected void invalidated() {
            updateGameBoard();
        }
    };

    public GameBoard getGameBoard() {
        return gameBoard.get();
    }

    public void setGameBoard(GameBoard gameBoard) {
        this.gameBoard.set(gameBoard);
    }

    public ObjectProperty<GameBoard> gameBoardProperty() {
        return gameBoard;
    }

    private void updateGameBoard() {
        ai = new Ai(aiPlayer);
        ai.setListener(new Ai.AiListener() {
            @Override
            public void onStart() {
                boardGridPane.setDisable(true);
                aiProgress.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            }

            @Override
            public void onFinish() {
                boardGridPane.setDisable(false);
                aiProgress.setProgress(0);
            }
        });

        boardGridPane.getChildren().clear();

        GameBoard gameBoard = gameBoardProperty().getValue();

        gameBoard.setOnFieldChangedListener(new OnFieldChangedListener() {
            @Override
            public void onFieldChanged(Move move, FieldMark fieldMark) {
                GridPaneUtils.getNodeByRowColumnIndex(boardGridPane, move.getRow() + 1, move.getColumn() + 1).ifPresent(node -> {
                    Button button = (Button) node;
                    button.setDisable(true);

                    if (fieldMark == FieldMark.CIRCLE) {
                        button.setGraphic(MaterialDesignIconFactory.get().createIcon(MaterialDesignIcon.RADIOBOX_BLANK));
                    } else if (fieldMark == FieldMark.CROSS) {
                        button.setGraphic(MaterialDesignIconFactory.get().createIcon(MaterialDesignIcon.CLOSE));
                    }
                });
            }
        });

        String style = String.format("-fx-font-size: %d", 300 / gameBoardProperty().getValue().size());

        for (int i = 1; i <= gameBoard.size(); i++) {
            Label row = new Label(String.valueOf(i));
            Label column = new Label(String.valueOf(i));

            boardGridPane.add(row, 0, i);
            boardGridPane.add(column, i, 0);

            GridPane.setFillWidth(row, true);
            GridPane.setFillHeight(row, true);

            GridPane.setHgrow(row, Priority.ALWAYS);
            GridPane.setVgrow(row, Priority.ALWAYS);

            GridPane.setHalignment(row, HPos.CENTER);
            GridPane.setValignment(row, VPos.CENTER);

            GridPane.setFillWidth(column, true);
            GridPane.setFillHeight(column, true);

            GridPane.setHgrow(column, Priority.ALWAYS);
            GridPane.setVgrow(column, Priority.ALWAYS);

            GridPane.setHalignment(column, HPos.CENTER);
            GridPane.setValignment(column, VPos.CENTER);
        }

        for (int row = 1; row <= gameBoard.size(); row++) {
            for (int column = 1; column <= gameBoard.size(); column++) {
                Button button = new Button();
                button.getStyleClass().addAll("button-raised", "button-game-board");
                button.setGraphic(filler());
                button.setStyle(style);

                button.setOnAction(event -> {
                    Object source = event.getSource();

                    if (source instanceof Button) {
                        Move move = (Move) button.getUserData();
                        gameBoard.setFieldMark(move, humanPlayer);

                        log.info("Move: {}", button.getUserData());

                        gameBoard.checkGameWinner().ifPresent(new Consumer<GameWinner>() {
                            @Override
                            public void accept(GameWinner gameWinner) {
                                log.info("{}", gameWinner);
                            }
                        });

                        ai.performMove(gameBoardProperty().getValue());
                    }
                });

                button.setUserData(new Move(row - 1, column - 1));

                boardGridPane.add(button, column, row);

                GridPane.setFillWidth(button, true);
                GridPane.setFillHeight(button, true);

                GridPane.setHgrow(button, Priority.SOMETIMES);
                GridPane.setVgrow(button, Priority.SOMETIMES);

                GridPane.setHalignment(button, HPos.CENTER);
                GridPane.setValignment(button, VPos.CENTER);
            }
        }
    }

    @Override
    public void initialize() {
        String filePath = "input";
        FieldMark next = null;

        GameBoard gameBoard = new GameBoard(BOARD_SIZE);
        gameBoardProperty().setValue(gameBoard);
        boardGridPane.getStyleClass().add("game-board");

        try {
            Pair<List<Pair<FieldMark, Move>>, FieldMark> data = GameBoardStateParser.parse(Files.lines(Paths.get(filePath)), BOARD_SIZE);

            List<Pair<FieldMark, Move>> moves = data.getKey();

            for (Pair<FieldMark, Move> move : moves) {
                gameBoard.setFieldMark(move.getValue(), move.getKey());
            }

        } catch (IOException | GameBoardStateParserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSceneAttach(GameBoardView view, Scene scene, Window window) {
        Stage stage = (Stage) window;

        if (stage.titleProperty().isBound()) {
            stage.titleProperty().unbind();
        }

        stage.titleProperty().bind(view.titleProperty());
    }
}
