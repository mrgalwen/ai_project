package org.bitbucket.mrgalwen.aiproject.lib.mvpfx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * @author Michał Kozioł
 * @since 2016-09-27
 */
@SuppressWarnings("unused")
public class Nodes {
    /**
     * Reloads stylesheets on {@code scene}
     *
     * @param scene for which stylesheets should be reloaded
     */
    public static void reloadStylesheets(Scene scene) {
        ObservableList<String> styles = FXCollections.observableArrayList(scene.getStylesheets());
        scene.getStylesheets().clear();
        scene.getStylesheets().addAll(styles);
    }

    /**
     * Reloads stylesheets on {@code stage}
     *
     * @param stage for which stylesheets should be reloaded
     */
    public static void reloadStylesheetsOnFocus(Stage stage) {
        stage.focusedProperty().addListener((observable, oldValue, focused) -> {
            if (focused && FX.isInitialized()) {
                reloadStylesheets(stage.getScene());
            }
        });
    }

    /**
     * Reloads stylesheets on {@code pane}
     *
     * @param pane for which stylesheets should be reloaded
     */
    public static void reloadStylesheets(Pane pane) {
        ObservableList<String> styles = FXCollections.observableArrayList(pane.getStylesheets());
        pane.getStylesheets().clear();
        pane.getStylesheets().addAll(styles);
    }
}
