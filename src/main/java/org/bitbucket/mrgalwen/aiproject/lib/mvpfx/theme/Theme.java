package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme;

import com.sun.javafx.css.StyleManager;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception.ThemeInfoParsingException;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception.ThemeLoadingException;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.util.StringUtils;
import org.bitbucket.mrgalwen.aiproject.lib.util.PlatformUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.matchprocessor.FileMatchProcessor;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.Parent;
import javafx.scene.Scene;
import lombok.extern.slf4j.Slf4j;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

/**
 * {@code Theme} is a {@code final class} responsible for handling MvpFX themes. This class contains
 * various static methods for loading and manipulating stylesheets of JavaFX application.
 * <p>
 * Themes can be loaded from: <ul> <li>Resources: {@code "/resources/theme/"}</li> <li>Assets (from
 * application directory): {@code System.getProperty("user.dir") + "/assets/"}</li> </ul>
 *
 * @author Michał Kozioł
 * @since 2016-09-28
 */
@Slf4j
@SuppressWarnings("unused")
public final class Theme {

    /**
     * MvpFX default theme name, use this name for your default theme (located at {@code
     * /resources/theme/main}
     */
    public static final String MAIN_THEME_NAME = "main";

    private static final String RESOURCE_THEME = "theme/";
    private static final String CSS_PATTERN = "/[a-zA-Z0-9]+\\.css";

    private static final ObservableList<String> STYLESHEETS = FXCollections.observableArrayList();
    private static final ReadOnlyListWrapper<String> STYLESHEETS_WRAPPER = new
            ReadOnlyListWrapper<>(STYLESHEETS);
    private static final String CSS_EXTENSION = ".css";
    private static ThemeInfo themeInfo;

    private static ThemeWatchTask watchTask;
    private static Executor executor = Executors.newSingleThreadExecutor();

    static {
        STYLESHEETS.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(Change<? extends String> c) {
                try {
                    PlatformUtils.runAndWait(() -> {
                        Application.setUserAgentStylesheet(null);

                        if (c.next()) {
                            for (String added : c.getAddedSubList()) {
                                StyleManager.getInstance().addUserAgentStylesheet(added);
                            }

                            for (String removed : c.getRemoved()) {
                                StyleManager.getInstance().removeUserAgentStylesheet(removed);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Theme() {
    }

    public static ReadOnlyListProperty<String> getStylesheets() {
        return STYLESHEETS_WRAPPER.getReadOnlyProperty();
    }

    /**
     * Loads theme using specific {@link ThemeInfo}
     *
     * @param themeInfo that contains information about loaded theme
     *
     * @return {@code true} if the was successfully loaded, otherwise returns {@code false}
     */
    public static boolean load(ThemeInfo themeInfo) {
        String themeName = themeInfo.getName();

        try {
            return loadFromAssets(themeName) || loadFromResources(themeName);
        } catch (ThemeLoadingException e) {
            log.info(e.getMessage(), e);
        }

        return false;
    }

    /**
     * Reloads currently loaded theme
     *
     * @return {@code true} if the was successfully reloaded, otherwise returns {@code false}
     */
    public static boolean reload() {
        return isLoaded() && load(themeInfo);
    }

    public static boolean isLoaded() {
        return Objects.nonNull(themeInfo);
    }

    /**
     * Loads theme with specified {@code themeName} from application resources
     *
     * @param themeName is a simple theme name (name of theme directory)
     *
     * @return {@code true} if theme was loaded successfully (at least one {@code .css} file was
     * loaded), otherwise returns {@code false}
     *
     * @throws ThemeLoadingException when theme path not exists or {@code info.properties} was not
     *                               found
     */
    public static boolean loadFromResources(String themeName) throws ThemeLoadingException {
        String themePath = RESOURCE_THEME + themeName;
        String themeInfoPath = themePath + "/info.properties";

        String pattern = themePath + CSS_PATTERN;

        try {
            ThemeInfo themeInfo = ThemeInfo.fromResources(themeInfoPath);

            ObservableList<String> stylesheets = FXCollections.observableArrayList();

            FastClasspathScanner classpathScanner = new FastClasspathScanner();
            classpathScanner.matchFilenamePattern(pattern, new FileMatchProcessor() {
                @Override
                public void processMatch(String relativePath, InputStream inputStream, long
                        lengthBytes) throws IOException {

                    if (relativePath.startsWith("/")) {
                        relativePath = "/" + relativePath;
                    }

                    importStylesheet(relativePath, stylesheets);
                }
            });

            classpathScanner.scan();

            if (!stylesheets.isEmpty()) {
                STYLESHEETS.clear();
                STYLESHEETS.addAll(stylesheets);
                Theme.themeInfo = themeInfo;

                log.info("Theme '{}' loaded from application resources: {}", themeName, themePath);

                return true;
            }

        } catch (ThemeInfoParsingException e) {
            throw new ThemeLoadingException(e);
        }

        return false;
    }

    /**
     * Loads theme with specified {@code themeName} from application assets directory
     *
     * @param themeName is a simple theme name (name of theme directory)
     *
     * @return {@code true} if theme was loaded successfully (at least one {@code .css} file was
     * loaded), otherwise returns {@code false}
     *
     * @throws ThemeLoadingException when theme path not exists or {@code info.properties} was not
     *                               found
     */
    public static boolean loadFromAssets(String themeName) throws ThemeLoadingException {
        Path themePath = Paths.get("assets", "theme", themeName);
        themePath = themePath.toAbsolutePath();
        Path themeInfoPath = Paths.get(themePath.toString(), "info.properties");

        try {
            ThemeInfo themeInfo = ThemeInfo.fromPath(themeInfoPath);
            ObservableList<String> stylesheets = FXCollections.observableArrayList();

            try {
                stylesheets.addAll(Files.find(themePath, 1, (path, basicFileAttributes) -> path
                        .getFileName().toString().endsWith(".css"))
                                           .map(Path::toUri)
                                           .map(URI::toString)
                                           .collect(Collectors.toList()));

                if (!stylesheets.isEmpty()) {
                    STYLESHEETS.clear();
                    STYLESHEETS.addAll(stylesheets);
                    Theme.themeInfo = themeInfo;

                    log.info("Theme '{}' loaded from assets directory: {}", themeName, themePath);

                    registerAssetsThemeWatchService(themePath);

                    return true;
                }
            } catch (IOException e) {
                throw new ThemeLoadingException(e);
            }
        } catch (ThemeInfoParsingException e) {
            throw new ThemeLoadingException(e);
        }

        return false;
    }

    /**
     * Applies theme stylesheets to given {@link Scene} object
     *
     * @param scene for which stylesheets should be applied
     */
    public static void applyStylesheetsTo(Scene scene) {
        scene.getStylesheets().addAll(STYLESHEETS);

        STYLESHEETS.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(Change<? extends String> it) {
                while (it.next()) {
                    if (it.wasAdded()) {
                        it.getAddedSubList()
                                .forEach((Consumer<String>) s -> scene.getStylesheets().add(s));
                    }

                    if (it.wasRemoved()) {
                        it.getRemoved()
                                .forEach((Consumer<String>) s -> scene.getStylesheets().remove(s));
                    }
                }
            }
        });
    }

    /**
     * Applies theme stylesheets to given {@link Parent} object
     *
     * @param parent for which stylesheets should be applied
     */
    public static void applyStylesheetsTo(Parent parent) {
        parent.getStylesheets().addAll(STYLESHEETS);

        STYLESHEETS.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(Change<? extends String> it) {
                while (it.next()) {
                    if (it.wasAdded()) {
                        it.getAddedSubList()
                                .forEach((Consumer<String>) s -> parent.getStylesheets().add(s));
                    }

                    if (it.wasRemoved()) {
                        it.getRemoved()
                                .forEach((Consumer<String>) s -> parent.getStylesheets().remove(s));
                    }
                }
            }
        });
    }

    /**
     * Finds and returns available application themes
     *
     * @return list of available themes
     */
    public static List<ThemeInfo> findAvailableThemes() {
        Set<ThemeInfo> themes = new HashSet<>();
        Path assetsPath = Paths.get("assets", "theme").toAbsolutePath();

        try {
            Set<ThemeInfo> themeInfoSet = Files
                    .find(assetsPath, 1,
                          (path, basicFileAttributes) -> !assetsPath.equals(path) && !StringUtils
                                  .isNullOrEmpty(path.getFileName().toString()))
                    .map(path -> {
                        try {
                            return ThemeInfo
                                    .fromPath(Paths.get(path.toString(), "info.properties"));
                        } catch (ThemeInfoParsingException e) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            themes.addAll(themeInfoSet);

            log.info("");
        } catch (IOException e) {
            log.info(e.getMessage());
        }

        return new ArrayList<>(themes);
    }

    /**
     * Imports given stylesheet from resources to current theme
     *
     * @param stylesheet that should be imported
     */
    public static void importStylesheet(String stylesheet) {
        importStylesheet(stylesheet, STYLESHEETS);
    }

    /**
     * Imports given stylesheet from resources to current theme
     *
     * @param stylesheetUrl is an url of stylesheet that should be imported
     */
    public static void importStylesheet(URL stylesheetUrl) {
        importStylesheet(stylesheetUrl, STYLESHEETS);
    }

    private static void importStylesheet(String stylesheet, ObservableList<String> destination) {
        URL css = Theme.class.getResource(stylesheet);
        importStylesheet(css, destination);
    }

    private static void importStylesheet(URL stylesheetUrl, ObservableList<String> destination) {
        destination.add(stylesheetUrl.toExternalForm());
    }

    /**
     * Returns <code>Optional</code> describing the value of currently loaded {@link ThemeInfo},
     * otherwise return an empty <code>Optional</code>.
     *
     * @return <code>Optional</code> describing the value of currently loaded {@link ThemeInfo},
     * otherwise return an empty <code>Optional</code>.
     */
    public static Optional<ThemeInfo> getThemeInfo() {
        return Optional.ofNullable(themeInfo);
    }

    private static void registerAssetsThemeWatchService(Path themePath) {
        if (watchTask == null) {
            watchTask = new ThemeWatchTask();

            watchTask.registerFileSystemListener(new DirectoryListener() {
                @Override
                public void onFileModified(Path path) {
                    log.debug("File modified {}", path);

                    String css = path.toUri().toString();

                    STYLESHEETS.remove(css);
                    STYLESHEETS.add(css);
                }

                @Override
                public void onFileCreated(Path path) {
                    log.debug("File created {}", path);

                    String css = path.toUri().toString();
                    STYLESHEETS.add(css);
                }

                @Override
                public void onFileDeleted(Path path) {
                    log.debug("File deleted {}", path);

                    String css = path.toUri().toString();
                    STYLESHEETS.remove(css);
                }
            });

            executor.execute(watchTask);
        }

        Optional<Path> pathOptional = watchTask.getPath();

        if (!pathOptional.isPresent() || !pathOptional.get().equals(themePath)) {
            watchTask.watchDirectory(themePath);
        }
    }

    private static class ThemeWatchTask extends Task<Void> {
        private WatchService watchService;
        private WatchKey watchKey;
        private Path path;
        private DirectoryListener directoryListener;

        public ThemeWatchTask() {
            try {
                watchService = FileSystems.getDefault().newWatchService();
            } catch (IOException e) {
                log.error("Could not create WatchService!", e);
            }
        }

        public void registerFileSystemListener(DirectoryListener directoryListener) {
            this.directoryListener = directoryListener;
        }

        @Override
        protected Void call() throws Exception {
            for (; ; ) {

                if (isCancelled()) {
                    return null;
                }

                WatchKey key;

                try {
                    key = watchService.take();
                } catch (InterruptedException x) {
                    return null;
                }

                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();

                    if (kind == OVERFLOW) {
                        continue;
                    }

                    @SuppressWarnings("unchecked")
                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    Path fileNamePath = ev.context();

                    Path child = path.resolve(fileNamePath);
                    String fileName = child.getFileName().toString();

                    if (fileName.toLowerCase().endsWith(CSS_EXTENSION)) {
                        log.debug("File changed '{}'!", child);

                        Platform.runLater(() -> {
                            if (Objects.nonNull(directoryListener)) {
                                if (kind == ENTRY_MODIFY) {
                                    directoryListener.onFileModified(child);

                                } else if (kind == ENTRY_CREATE) {
                                    directoryListener.onFileCreated(child);

                                } else if (kind == ENTRY_DELETE) {
                                    directoryListener.onFileDeleted(child);
                                }
                            }
                        });
                    }
                }

                key.reset();
            }
        }

        public void watchDirectory(Path directory) {
            synchronized (this) {
                if (Objects.nonNull(watchKey)) {
                    watchKey.cancel();
                }

                try {
                    watchKey = directory
                            .register(watchService, ENTRY_MODIFY, ENTRY_DELETE, ENTRY_CREATE);
                    path = directory;

                    log.info("Successfully registered WatchService for path: {}", directory);
                } catch (IOException e) {
                    log.error("Could not register WatchService for path: {}", directory, e);
                }
            }
        }

        public Optional<Path> getPath() {
            return Optional.ofNullable(path);
        }

        public void setPath(Path path) {
            this.path = path;
        }
    }
}
