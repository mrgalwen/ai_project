package org.bitbucket.mrgalwen.aiproject.lib.util;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javafx.application.Platform;

/**
 * @author Michał Kozioł
 * @since 2016-08-29
 */
@SuppressWarnings("unused")
public class PlatformUtils {
    public static void runAndWait(Runnable runnable) throws Exception {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
        } else {
            FutureTask<?> future = new FutureTask<Void>(runnable, null);
            Platform.runLater(future);
            future.get();
        }
    }

    public static <T> T runAndWait(Callable<T> callable) throws Exception {
        if (Platform.isFxApplicationThread()) {
            return callable.call();
        } else {
            FutureTask<T> future = new FutureTask<>(callable);
            Platform.runLater(future);
            return future.get();
        }
    }

    public static void forceExit() {
        Platform.exit();
        System.exit(0);
    }
}
