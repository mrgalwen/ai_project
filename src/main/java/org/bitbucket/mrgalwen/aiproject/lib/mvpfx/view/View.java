package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.view;

import com.codepoetics.navn.Name;

import org.springframework.beans.factory.annotation.Autowired;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.FX;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.presenter.Presenter;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;

import static java.util.ResourceBundle.getBundle;
import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * @author Michał Kozioł
 */
@Slf4j
@SuppressWarnings("unused")
public abstract class View<P extends Presenter> extends StackPane {
    public final static String DEFAULT_SUFFIX = "View";
    protected final static ExecutorService PARENT_CREATION_POOL = getExecutorService();
    private static final String RESOURCE_VIEW = "/view";
    protected static Executor FX_PLATFORM_EXECUTOR = Platform::runLater;
    protected ObjectProperty<P> presenterProperty;
    protected String bundleName;
    protected ResourceBundle bundle;
    protected URL resource;
    private SimpleStringProperty title = new SimpleStringProperty();

    @Autowired
    private FXMLLoader fxmlLoader;

    /**
     * Constructs the view without loading FXML.
     */
    public View() {
        this((String) null);
    }

    /**
     * Constructs the view without loading FXML.
     *
     * @param title of this view
     */
    public View(String title) {
        init(getFXMLName());
        titleProperty().setValue(title);
    }

    public View(StringBinding titlePropertyBinding) {
        init(getFXMLName());
        titleProperty().bind(titlePropertyBinding);
    }

    public static ResourceBundle getResourceBundle(String name) {
        try {
            return getBundle(name);
        } catch (MissingResourceException ex) {
            return null;
        }
    }

    protected static ExecutorService getExecutorService() {
        return Executors.newCachedThreadPool((r) -> {
            Thread thread = Executors.defaultThreadFactory().newThread(r);
            String name = thread.getName();
            thread.setName("MvpFX" + name);
            thread.setDaemon(true);
            return thread;
        });
    }

    protected void onViewLoaded() {

    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    private void init(final String conventionalName) {
        this.presenterProperty = new SimpleObjectProperty<>();
        this.resource = getClass().getResource(conventionalName);
        this.bundleName = getBundleName();
        this.bundle = getResourceBundle(bundleName);
    }

    private void loadSynchronously(final URL resource, ResourceBundle bundle,
                                   final String conventionalName) throws IllegalStateException {
        if (presenterProperty.isNull().get()) {

            fxmlLoader.setLocation(resource);
            fxmlLoader.setResources(bundle);

            try {
                fxmlLoader.load();
                presenterProperty.set(fxmlLoader.getController());
                onViewLoaded();
            } catch (IOException ex) {
                throw new IllegalStateException("Cannot load " + conventionalName, ex);
            }
        }
    }

    /**
     * Initializes the view by loading the FXML (if not happened yet) and
     * returns the top Node (parent) specified in
     *
     * @return the node loaded by FXMLLoader
     */
    public Parent getView() {
        loadSynchronously(resource, bundle, bundleName);

        Parent root = fxmlLoader.getRoot();
        addStyleSheetIfAvailable(root);

        root.sceneProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue == null && newValue != null) {
                onAttach(newValue);
            } else if (oldValue != null && newValue == null) {
                onDetach(oldValue);
            } else if (oldValue != null) {
                onDetach(oldValue);
                onAttach(newValue);
            }
        });

        root.visibleProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue == null && newValue != null) {
                onShow();
            } else if (oldValue != null && newValue == null) {
                onHide();
            }
        });
        return root;
    }

    /**
     * Initializes the view synchronously and invokes and passes the created
     * parent Node to the consumer within the FX UI thread.
     *
     * @param consumer - an object interested in received the Parent as callback
     */
    public void getView(Consumer<Parent> consumer) {
        Supplier<Parent> supplier = this::getView;
        supplyAsync(supplier, FX_PLATFORM_EXECUTOR).
                thenAccept(consumer).
                exceptionally(this::exceptionReporter);
    }

    /**
     * Creates the view asynchronously using an internal thread pool and passes
     * the parent node within the UI Thread.
     *
     * @param consumer - an object interested in received the Parent as callback
     */
    public void getViewAsync(Consumer<Parent> consumer) {
        Supplier<Parent> supplier = this::getView;

        CompletableFuture.supplyAsync(supplier, PARENT_CREATION_POOL).
                thenAcceptAsync(consumer, FX_PLATFORM_EXECUTOR).
                exceptionally(this::exceptionReporter);
    }

    /**
     * Scene Builder creates for each FXML document a root container. This method omits the root
     * container (e.g. AnchorPane) and gives access to its first child.
     *
     * @return An {@link Optional} that contains the node.
     */
    public Optional<Node> getViewWithoutRootContainer() {
        final ObservableList<Node> children = getView().getChildrenUnmodifiable();

        if (children.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(children.listIterator().next());
    }

    protected void addStyleSheetIfAvailable(Parent parent) {
        FX.applyStylesheetsTo(parent);

        URL uri = getClass().getResource(getStyleSheetName());

        if (uri == null) {
            return;
        }

        String uriToCss = uri.toExternalForm();
        parent.getStylesheets().add(uriToCss);
    }

    protected String getStyleSheetName() {
        return getResourceCamelOrLowerCase(RESOURCE_VIEW, false, ".css");
    }

    /**
     * @return the name of the fxml file derived from the FXML view. e.g. The name for the
     * WelcomeScreenView is going to be WelcomeScreen.fxml or welcome_screen.fxml.
     */
    protected final String getFXMLName() {
        return getResourceCamelOrLowerCase(RESOURCE_VIEW, true, ".fxml");
    }

    protected String getResourceCamelOrLowerCase(String location, boolean mandatory, String
            ending) {
        if (!StringUtils.isNullOrEmpty(location)) {
            if (!location.endsWith("/")) {
                location += "/";
            }
        }

        if (Objects.isNull(location)) {
            location = "";
        }

        String name = location + getConventionalName(true, ending);
        URL found = getClass().getResource(name);

        if (found != null) {
            return name;
        }

        log.error("File: " + name + " not found, attempting with camel case");

        name = location + getConventionalName(false, ending);
        found = getClass().getResource(name);

        if (mandatory && found == null) {
            final String message = "Cannot load file " + name;
            log.error(message);
            log.error("Stopping initialization phase...");
            throw new IllegalStateException(message);
        }

        return name;
    }

    /**
     * In case the view was not initialized yet, the conventional fxml
     * (welcome_screen.fxml for the WelcomeScreenView and WelcomeScreenPresenter) are loaded and
     * the specified presenter / controller is going to be constructed and
     * returned.
     *
     * @return the corresponding controller / presenter (usually for a WelcomeScreenView the
     * WelcomeScreenPresenter)
     */
    public P getPresenter() {
        loadSynchronously(resource, bundle, bundleName);
        return this.presenterProperty.get();
    }

    /**
     * Does not initialize the view. Only registers the Consumer and waits until
     * the the view is going to be created / the method View#getView or
     * View#getViewAsync invoked.
     *
     * @param presenterConsumer listener for the presenter construction
     */
    public void getPresenter(Consumer<P> presenterConsumer) {
        this.presenterProperty.addListener(
                (observable, oldValue, newValue) -> presenterConsumer.accept(newValue));
    }

    /**
     * @param lowercase indicates whether the simple class name should be converted to lowercase of
     *                  left unchanged
     * @param suffix    the suffix to append
     *
     * @return the conventional name with truncated suffix
     */
    protected String getConventionalName(boolean lowercase, String suffix) {
        return getConventionalName(lowercase) + suffix;
    }

    /**
     * @param lowercase indicates whether the simple class name should be
     *
     * @return the name of the view without the "View" prefix.
     */
    protected String getConventionalName(boolean lowercase) {
        final String clazzWithEnding = this.getClass().getSimpleName();
        String clazz = StringUtils.truncateSuffix(clazzWithEnding, DEFAULT_SUFFIX);

        if (lowercase) {
            Name name = Name.of(clazz);
            clazz = name.toUnderscored();
        }

        return clazz;
    }

    protected String getBundleName() {
        String conventionalName = getConventionalName(true);
        return this.getClass().getPackage().getName() + "." + conventionalName;
    }

    /**
     * @return an existing resource bundle, or null
     */
    public ResourceBundle getResourceBundle() {
        return this.bundle;
    }

    /**
     * @param t exception to report
     *
     * @return nothing
     */
    public Void exceptionReporter(Throwable t) {
        log.error(t.toString());
        return null;
    }

    @SuppressWarnings("unchecked")
    protected void onAttach(Scene scene) {
        P presenter = getPresenter();
        presenter.onViewAttach(this, scene);
    }

    @SuppressWarnings("unchecked")
    protected void onDetach(Scene scene) {
        P presenter = getPresenter();
        presenter.onViewAttach(this, scene);
    }

    @SuppressWarnings("unchecked")
    protected void onShow() {
        P presenter = getPresenter();
        presenter.onViewShow(this);
    }

    @SuppressWarnings("unchecked")
    protected void onHide() {
        P presenter = getPresenter();
        presenter.onViewHide(this);
    }
}
