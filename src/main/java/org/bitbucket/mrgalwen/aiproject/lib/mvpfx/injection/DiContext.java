package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection;

/**
 * Common definition of Dependecy Injection Context
 *
 * @author Michał Kozioł
 * @since 06.10.2016
 */
public interface DiContext {

    /**
     * Injects members into given instance
     *
     * @param instance instance to inject members into
     */
    void injectMembers(Object instance);

    /**
     * Creates new instance of given class and injects all members
     *
     * @param clazz is a class which instance will be used to object instance creation
     * @param <T> is a type of an created object
     * @return resulting instance
     */
    <T> T newInstance(Class<T> clazz);

    /**
     * Context initialization
     */
    void init();

    /**
     * Context disposal
     */
    void dispose();
}