

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.presenter;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.view.View;

import javafx.scene.Scene;
import javafx.stage.Window;

/**
 * @author Michał Kozioł
 * @since 18.09.2016
 */
@SuppressWarnings("unused")
public interface Presenter<V extends View> {
    void initialize();

    default void onViewAttach(V view, Scene scene) {
    }

    default void onViewDetach(V view, Scene scene) {
    }

    default void onViewShow(V view) {
    }

    default void onViewHide(V view) {
    }

    default void onSceneAttach(V view, Scene scene, Window window) {
    }

    default void onSceneDetach(V view, Scene scene, Window window) {
    }
}
