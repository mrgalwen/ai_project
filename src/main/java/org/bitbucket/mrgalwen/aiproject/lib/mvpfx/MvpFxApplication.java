package org.bitbucket.mrgalwen.aiproject.lib.mvpfx;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.view.View;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection.DiContext;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection.spring.SpringDiContext;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * {@code MvpFxApplication} is an abstract class for Java FX MVP application.
 *
 * @param <PrimaryView> is a primary view class that inherits from {@link View}, this view is loaded
 *                      on application start
 *
 * @author Michał Kozioł
 * @since 2016-09-19
 */
@SuppressWarnings("unused")
public abstract class MvpFxApplication<PrimaryView extends View> extends Application {
    private final DiContext context;

    /**
     * Basic constructor.
     */
    public MvpFxApplication() {
        this(null);
    }

    /**
     * Constructor that adds an icon to primary stage
     *
     * @param icon for primary scene
     */
    public MvpFxApplication(Image icon) {
        context = new SpringDiContext(this, () -> Arrays
                .asList(MvpFxApplication.class.getPackage().getName(),
                        this.getClass().getPackage().getName()));

        if (Objects.nonNull(icon)) {
            FX.setStageIcon(icon);
        }
    }

    @Override
    public final void init() throws Exception {
        initMvpFx();
    }

    protected void initMvpFx() {
    }

    /**
     * Returns dependency injection context
     *
     * @return {@link DiContext} implementation instance
     */
    public DiContext getContext() {
        return context;
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void start(Stage primaryStage) throws Exception {
        try {
            FX.registerApplication(this, primaryStage);

            context.init();

            Class<PrimaryView> viewClass = (Class<PrimaryView>) ((ParameterizedType) getClass()
                    .getGenericSuperclass())
                    .getActualTypeArguments()[0];

            PrimaryView view = getContext().newInstance(viewClass);

            primaryStage.setScene(createPrimaryScene(view));
            primaryStage.show();
            FX.setInitialized(true);

            startMvpFx(primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * This method is invoked after primary scene creation
     *
     * @param stage is application primary {@code Stage}
     */
    protected abstract void startMvpFx(Stage stage);

    /**
     * Creates primary scene
     *
     * @param view a primary {@code Scene} view instance
     *
     * @return a primary {@code Scene}
     */
    protected Scene createPrimaryScene(View view) {
        return new Scene(view.getView());
    }

    /**
     * Creates primary view
     *
     * @param viewClass a primary {@code View} type
     *
     * @return a primary {@code View}
     */
    protected PrimaryView createPrimaryView(Class<PrimaryView> viewClass) {
        return getContext().newInstance(viewClass);
    }

    protected List<String> getScanPackages() {
        return new ArrayList<>();
    }
}
