package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.i18n;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.matchprocessor.FileMatchProcessor;
import io.github.lukehutch.fastclasspathscanner.matchprocessor.FileMatchProcessorWithContext;
import lombok.extern.slf4j.Slf4j;

import static java.util.stream.Collectors.toList;

/**
 * @author Michał Kozioł
 * @since 2016-09-02
 */
@Slf4j
@SuppressWarnings("unused")
public class ResourceBundleMessageProvider extends AbstractMessageProvider {
    private static final String PREFIX = "i18n/";
    private static final String TAG_NAME_REGEX = "[a-zA-Z0-9_]+_";
    private static final String FILE_NAME_EXTENSION = ".properties";
    private static final String BUNDLE_PREFIX = "i18n.";

    private Map<String, String> tagToBundleLocationStore = new HashMap<>();

    public ResourceBundleMessageProvider() {
        super();
    }

    private ResourceBundle getClassPathBundle(String baseName) {
        return ResourceBundle.getBundle(baseName, getLocale(), new Utf8Control());
    }

    @Override
    public String getText(String tag, String textId, String defaultText, Object... arguments) {
        if (tagToBundleLocationStore.containsKey(tag)) {
            String baseName = tagToBundleLocationStore.get(tag);
            ResourceBundle resourceBundle = getClassPathBundle(baseName);

            String text;

            if (resourceBundle.containsKey(textId)) {
                text = resourceBundle.getString(textId);

                if (text.isEmpty()) {
                    if (StringUtils.isNullOrEmpty(defaultText)) {
                        return textId;
                    } else {
                        text = defaultText;
                    }
                }
            } else {
                if (StringUtils.isNullOrEmpty(defaultText)) {
                    return textId;
                } else {
                    text = defaultText;
                }
            }

            if (arguments.length > 0) {
                return String.format(text, arguments);
            }

            return text;
        }

        return textId;
    }

    @Override
    @SuppressWarnings("all")
    public void load() {
        tagToBundleLocationStore.clear();

        FastClasspathScanner fastClasspathScanner = new FastClasspathScanner();

        String pathRegexp = retrieveClassPathRegexp();
        final String fileNameSuffix = retrieveFileNameSuffix();

        fastClasspathScanner.matchFilenamePattern(pathRegexp, new FileMatchProcessorWithContext() {
            @Override
            public void processMatch(File classpathElt, String relativePath, InputStream
                    inputStream, long lengthBytes) throws IOException {
                Path path = Paths.get(relativePath);
                String fileName = path.getFileName().toString();

                int endIndex = fileName.length() - fileNameSuffix.length();
                String bundleName = fileName.substring(0, endIndex);

                tagToBundleLocationStore.put(bundleName, BUNDLE_PREFIX + bundleName);
            }
        });

        fastClasspathScanner.scan();
    }

    @Override
    protected List<Locale> retrieveSupportedLocales() {
        Set<Locale> supportedLocales = new HashSet<>();

        FastClasspathScanner classpathScanner = new FastClasspathScanner();
        classpathScanner.matchFilenamePattern(retrieveClassPathRegexp(), new FileMatchProcessor() {
            @Override
            public void processMatch(String relativePath, InputStream inputStream, long
                    lengthBytes) throws IOException {

                localeFromPath(Paths.get(relativePath))
                        .ifPresent(supportedLocales::add);

                log.info(relativePath);
            }
        });

        classpathScanner.scan();

        return new ArrayList<>(supportedLocales);
    }

    private Optional<Locale> localeFromPath(Path path) {
        String fileName = path.getFileName().toString();

        int first = fileName.indexOf("_") + 1;
        int last = fileName.indexOf(FILE_NAME_EXTENSION);

        String localeId = fileName.substring(first, last);

        @SuppressWarnings("all")
        List<Optional<Locale>> result = Arrays.asList(Optional.empty());

        Arrays.stream(Locale.getAvailableLocales())
                .forEach(locale -> {
                    if (localeId.equals(locale.toString())) {
                        result.set(0, Optional.of(locale));
                    }
                });

        return result.get(0);
    }

    protected String retrieveFileNameSuffix() {
        return "_" + localeProperty().get().toLanguageTag() + FILE_NAME_EXTENSION;
    }

    protected String retrieveClassPathRegexp() {
        String locales = String.join("|", Arrays.stream(Locale.getAvailableLocales())
                .map(Locale::toString)
                .collect(toList()));

        return PREFIX + TAG_NAME_REGEX + "(" + locales + ")" + "\\" + FILE_NAME_EXTENSION + "$";
    }
}
