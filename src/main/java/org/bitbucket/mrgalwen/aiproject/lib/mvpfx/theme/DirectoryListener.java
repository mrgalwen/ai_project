package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme;

import java.nio.file.Path;

/**
 * @author Michał Kozioł
 * @since 2016-11-07
 */
public interface DirectoryListener {
    void onFileModified(Path path);

    void onFileCreated(Path path);

    void onFileDeleted(Path path);
}
