package org.bitbucket.mrgalwen.aiproject.lib.mvpfx;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import lombok.extern.slf4j.Slf4j;

import static javafx.scene.control.Alert.AlertType.ERROR;

/**
 * @author Michał Kozioł
 * @since 2016-09-27
 */
@Slf4j
public class DefaultErrorHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable error) {
        //TODO move all text to I18n files
        log.error("Uncaught error", error);

        Platform.runLater(() -> {
            String text;

            if (error.getCause() != null && error.getCause().getLocalizedMessage() != null) {
                text = error.getLocalizedMessage();
            } else {
                text = "";
            }

            Label cause = new Label(text) {
                {
                    setStyle("-fx-font-weight: bold");
                }
            };

            TextArea textarea = new TextArea() {
                {
                    setPrefRowCount(20);
                    setPrefColumnCount(50);
                    setText(stringFromError(error));
                }
            };

            Alert alert = new Alert(ERROR) {
                {
                    setTitle("An error occured");
                    setResizable(true);
                    setHeaderText("Error in " + error.getStackTrace()[0].toString());
                    getDialogPane().setContent(new VBox(cause, textarea));
                    initModality(Modality.NONE);
                    show();
                }
            };
        });
    }

    private String stringFromError(Throwable e) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(out);
        e.printStackTrace(writer);
        writer.close();
        return out.toString();
    }
}
