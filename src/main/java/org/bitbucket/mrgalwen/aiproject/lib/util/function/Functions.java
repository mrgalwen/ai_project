package org.bitbucket.mrgalwen.aiproject.lib.util.function;

import net.jodah.typetools.TypeResolver;

import java.util.Objects;
import java.util.function.Function;

/**
 * This class contains various methods for manipulating {@link java.util.function.Function} objects.
 *
 * @author Michał Kozioł
 * @since 2016-08-17
 */
public final class Functions {
    private static final int GENERIC_ARGUMENT_TYPE_INDEX = 0;
    private static final int GENERIC_RETURN_TYPE_INDEX = 1;

    public static Class<?> getArgumentClass(Function function) {
        Objects.requireNonNull(function, "Function can't be null.");

        Class<?>[] classes = TypeResolver.resolveRawArguments(Function.class, function.getClass());
        return classes[GENERIC_ARGUMENT_TYPE_INDEX];
    }

    public static Class<?> getReturnClass(Function function) {
        Objects.requireNonNull(function, "Function can't be null.");

        Class<?>[] classes = TypeResolver.resolveRawArguments(Function.class, function.getClass());
        return classes[GENERIC_RETURN_TYPE_INDEX];
    }

    public static boolean hasCompatibleArgumentClass(Function function, Class<?> argumentClass) {
        Objects.requireNonNull(function, "Function can't be null.");
        Objects.requireNonNull(argumentClass, "Argument class can't be null.");

        Class<?> aClass = getArgumentClass(function);
        return aClass.isAssignableFrom(argumentClass);
    }

    public static boolean hasCompatibleReturnClass(Function function, Class<?> returnClass) {
        Objects.requireNonNull(function, "Function can't be null.");
        Objects.requireNonNull(returnClass, "Return class can't be null.");

        Class<?> aClass = getReturnClass(function);
        return aClass.isAssignableFrom(returnClass);
    }

    public static String toString(Function function) {
        Objects.requireNonNull(function, "Function can't be null.");

        Class<?> returnClass = getReturnClass(function);
        Class<?> argumentClass = getArgumentClass(function);

        return String.format("%s function(%s)", returnClass.getCanonicalName(), argumentClass.getCanonicalName());
    }
}
