

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception.ThemeInfoParsingException;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author Michał Kozioł
 * @since 2016-09-29
 */
public class ThemeInfo {
    public static final String KEY_NAME = "Theme.name";
    public static final String KEY_FULL_NAME = "Theme.fullName";

    private static final String THEME_INFO_SUFFIX = ".properties";

    private String name;
    private String fullName;

    public ThemeInfo(String name, String fullName) {
        this.name = name;
        this.fullName = fullName;
    }

    public static ThemeInfo fromResourceBundle(ResourceBundle bundle) throws
                                                                      ThemeInfoParsingException {
        if (bundle.containsKey(KEY_NAME) && bundle.containsKey(KEY_FULL_NAME)) {
            String name = bundle.getString(KEY_NAME);
            String fullName = bundle.getString(KEY_FULL_NAME);

            if (!StringUtils.isNullOrEmpty(name) && !StringUtils.isNullOrEmpty(fullName)) {
                return new ThemeInfo(name, fullName);
            }
        }

        throw new ThemeInfoParsingException();
    }

    public static ThemeInfo fromResources(String name) throws ThemeInfoParsingException {
        if (!name.startsWith("/")) {
            name = '/' + name;
        }

        URL resource = ThemeInfo.class.getResource(name);

        if (Objects.isNull(resource)) {
            throw new ThemeInfoParsingException();
        }

        return fromUrl(resource);
    }

    public static ThemeInfo fromUrl(URL url) throws ThemeInfoParsingException {
        Objects.requireNonNull(url);

        try {
            return fromUri(url.toURI());
        } catch (URISyntaxException e) {
            throw new ThemeInfoParsingException(e);
        }
    }

    public static ThemeInfo fromUri(URI uri) throws ThemeInfoParsingException {
        try (InputStream is = Files.newInputStream(Paths.get(uri))) {
            return fromInputStream(is);
        } catch (IOException e) {
            throw new ThemeInfoParsingException(e);
        }
    }

    public static ThemeInfo fromPath(Path path) throws ThemeInfoParsingException {
        return fromFile(path.toFile());
    }

    public static ThemeInfo fromFile(File file) throws ThemeInfoParsingException {
        try {
            String absolutePath = file.getAbsolutePath();

            if (!absolutePath.endsWith(THEME_INFO_SUFFIX)) {
                absolutePath += THEME_INFO_SUFFIX;
            }

            return fromInputStream(new FileInputStream(absolutePath));
        } catch (FileNotFoundException e) {
            throw new ThemeInfoParsingException(e);
        }
    }

    public static ThemeInfo fromInputStream(InputStream inputStream) throws
                                                                     ThemeInfoParsingException {
        try {
            return fromResourceBundle(new PropertyResourceBundle(inputStream));
        } catch (IOException e) {
            throw new ThemeInfoParsingException(e);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "ThemeInfo{" +
                "name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ThemeInfo)) return false;

        ThemeInfo themeInfo = (ThemeInfo) o;

        if (getName() != null ? !getName().equals(themeInfo.getName()) : themeInfo
                .getName() != null)
            return false;
        return getFullName() != null ? getFullName().equals(themeInfo.getFullName()) : themeInfo
                .getFullName() == null;

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getFullName() != null ? getFullName().hashCode() : 0);
        return result;
    }
}
