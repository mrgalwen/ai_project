package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.i18n;

import java.util.List;
import java.util.Locale;

import javafx.beans.property.ObjectProperty;

/**
 * @author Michał Kozioł
 * @since 2016-09-26
 */
public interface MessageProvider {
    List<Locale> getSupportedLocales();

    ObjectProperty<Locale> localeProperty();

    Locale getLocale();

    String getText(String tag, String textId, String defaultText, Object... arguments);

    String getText(String tag, String textId);

    String getText(String tag, String textId, Object... arguments);

    String getText(String tag, String textId, String defaultText);

    void load();
}
