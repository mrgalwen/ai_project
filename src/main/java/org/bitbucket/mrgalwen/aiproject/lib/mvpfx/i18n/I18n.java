package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.i18n;

import java.util.Locale;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import lombok.extern.slf4j.Slf4j;

/**
 * A {@code I18n} is an abstract class which represents {@code Locale} based set of translated
 * texts. I18n is searching for translation files in your resources. Translation files should be
 * placed in {@code i18n/TAG_LOCALE.EXTENSION} path, for example if we have {@code TAG} equals
 * {@code app}, {@code LOCALE} equals {@code pl_PL} and {@code EXTENSION} equals {@code
 * .properties}, the path will look like this: {@code i18n/app_pl_PL.properties}.
 *
 * @author Michał Kozioł
 * @since 2016-09-02
 */
@Slf4j
@SuppressWarnings("unused")
public final class I18n {
    private static final ObjectProperty<Locale> locale = new SimpleObjectProperty<>(Locale.getDefault());
    private static MessageProvider messageProvider;

    public static ObjectProperty<Locale> localeProperty() {
        return locale;
    }

    static {
        setMessageProvider(new ResourceBundleMessageProvider());
    }

    public static void setMessageProvider(MessageProvider messageProvider) {
        I18n.messageProvider = messageProvider;
        I18n.messageProvider.localeProperty().bind(localeProperty());
    }

    /**
     * Application I18N tag
     */
    public static final String APP = "app";

    /**
     * Common I18N tag
     */
    public static final String COMMON = "common";

    /**
     * Exception I18N tag
     */
    public static final String EXCEPTION = "exception";

    private I18n() {
    }

    public static Locale getLocale() {
        return locale.get();
    }

    public static void setLocale(Locale locale) {
        I18n.locale.setValue(locale);
    }

    /**
     * Gets stored translation by given {@code tag} and {@code textId}. If translation can't be
     * found, then returns {@code defaultText}
     *
     * @param tag         is internationalization file prefix
     * @param textId      is translation text identifier
     * @param defaultText is a text which is returned when translation can't be found
     * @return stored translation by given {@code tag} and {@code textId}, if translation can't be
     * found and {@code defaultText} is not null returns {@code defaultText}, otherwise returns
     * {@code textId}
     */
    public static String getText(String tag, String textId, String defaultText) {
        return messageProvider.getText(tag, textId, defaultText);
    }

    /**
     * Gets stored translation by given {@code tag} and {@code textId}, and uses {@link
     * String#format(String, Object...)} to format output using {@code arguments}. If translation
     * can't be found, then returns {@code defaultText}
     *
     * @param tag         is internationalization file prefix
     * @param textId      is translation text identifier
     * @param defaultText is a text which is returned when translation can't be found
     * @return stored translation by given {@code tag} and {@code textId}, if translation can't be
     * found and {@code defaultText} is not null returns {@code defaultText}, otherwise returns
     * {@code textId}
     */
    public static String getText(String tag, String textId, String defaultText, Object... arguments) {
        return messageProvider.getText(tag, textId, defaultText, arguments);
    }

    /**
     * Gets stored translation by given {@code tag} and {@code textId}. If translation can't be
     * found, then returns {@code textId}
     *
     * @param tag    is internationalization file prefix
     * @param textId is translation text identifier
     * @return stored translation by given {@code tag} and {@code textId}, if translation can't be
     * found returns {@code textId}
     */
    public static String getText(String tag, String textId) {
        return messageProvider.getText(tag, textId);
    }

    /**
     * Gets stored translation by given {@code tag} and {@code textId}, and uses {@link
     * String#format(String, Object...)} to format output using {@code arguments}. If translation
     * can't be found, then returns {@code textId}
     *
     * @param tag    is internationalization file prefix
     * @param textId is translation text identifier
     * @return stored translation by given {@code tag} and {@code textId}, if translation can't be
     * found returns {@code textId}
     */
    public static String getText(String tag, String textId, Object... arguments) {
        return messageProvider.getText(tag, textId, arguments);
    }

    public static StringBinding createBinding(String tag, String textId, String defaultText) {
        return Bindings.createStringBinding(() -> getText(tag, textId, defaultText), locale);
    }


    public static StringBinding createBinding(String tag, String textId, String defaultText, Object... arguments) {
        return Bindings.createStringBinding(() -> getText(tag, textId, defaultText, arguments), locale);
    }


    public static StringBinding createBinding(String tag, String textId) {
        return Bindings.createStringBinding(() -> getText(tag, textId), locale);
    }

    public static StringBinding createBinding(String tag, String textId, Object... arguments) {
        return Bindings.createStringBinding(() -> getText(tag, textId, arguments), locale);
    }
}
