

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception;

/**
 * @author Michał Kozioł
 * @since 2016-09-29
 */
@SuppressWarnings("unused")
public class ThemeLoadingException extends Exception {
    public ThemeLoadingException() {
    }

    public ThemeLoadingException(Throwable cause) {
        super(cause);
    }
}
