

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection.spring;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection.AbstractDiContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Supplier;

import javafx.fxml.FXMLLoader;

/**
 * {@code SpringDiContext} is an implementation of {@link AbstractDiContext}. This dependency
 * injection context is based on Spring Framework context.
 *
 * @author Michał Kozioł
 * @since 06.10.2016
 */
public class SpringDiContext extends AbstractDiContext {

    private final Object contextRoot;
    private AnnotationConfigApplicationContext appContext;

    private Supplier<Collection<String>> scanPackages;

    /**
     * Create the Spring context
     *
     * @param contextRoot  root object to inject
     * @param scanPackages packages to scan
     */
    public SpringDiContext(Object contextRoot, Supplier<Collection<String>> scanPackages) {
        this.contextRoot = Objects.requireNonNull(contextRoot);
        this.scanPackages = Objects.requireNonNull(scanPackages);
        this.appContext = new AnnotationConfigApplicationContext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void injectMembers(Object instance) {
        SpringUtils.injectMembers(appContext, instance);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T newInstance(Class<T> type) {
        return SpringUtils.newInstance(appContext, type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void init() {
        HashSet<String> packages = new HashSet<>();
        packages.addAll(scanPackages.get());

        appContext = new AnnotationConfigApplicationContext();
        appContext.register(FXModule.class);
        appContext.scan(packages.toArray(new String[0]));
        appContext.refresh();

        injectMembers(contextRoot);
    }
}

@Configuration
@SuppressWarnings("unused")
class FXModule implements ApplicationContextAware {

    private ApplicationContext appContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appContext = applicationContext;
    }

    @Bean
    @Scope("prototype")
    public FXMLLoader provideFxmlLoader() {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(type -> SpringUtils.newInstance(appContext, (Class<?>) type));
        return loader;
    }
}

class SpringUtils {

    private SpringUtils() {
    }

    public static void injectMembers(ApplicationContext appContext, Object instance) {
        appContext.getAutowireCapableBeanFactory().autowireBean(instance);
    }

    public static <T> T newInstance(ApplicationContext appContext, Class<T> type) {
        T instance = null;

        try {
            instance = type.newInstance();
            injectMembers(appContext, instance);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return instance;
    }
}
