

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.util;

import java.util.Objects;

/**
 * @author Michał Kozioł
 * @since 19.09.2016
 */
public final class StringUtils {
    public static String truncateSuffix(String string, String suffix) {
        Objects.requireNonNull(string);
        Objects.requireNonNull(suffix);

        if (string.endsWith(suffix)) {
            int viewIndex = string.indexOf(suffix);
            return string.substring(0, viewIndex);
        }

        return string;
    }

    public static String truncatePrefix(String string, String prefix) {
        Objects.requireNonNull(string);
        Objects.requireNonNull(prefix);

        if (string.startsWith(prefix)) {
            return string.substring(prefix.length());
        }

        return string;
    }

    public static boolean isNullOrEmpty(String string) {
        return Objects.isNull(string) || string.isEmpty();
    }
}
