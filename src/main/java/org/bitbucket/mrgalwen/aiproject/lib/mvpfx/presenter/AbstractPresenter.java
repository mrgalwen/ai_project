package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.presenter;

import com.kotcrab.annotation.CallSuper;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.view.View;

import java.util.Objects;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.stage.Window;

/**
 * @author Michał Kozioł
 * @since 2016-10-21
 */
public abstract class AbstractPresenter<V extends View<? extends Presenter>> implements Presenter<V> {
    private V view;
    private ObjectProperty<Scene> sceneProperty = new SimpleObjectProperty<>();

    private WindowChangeListener windowChangeListener = new WindowChangeListener();

    @Override
    @CallSuper
    public void onViewAttach(V view, Scene scene) {
        if (Objects.isNull(this.view)) {
            this.view = view;
        }

        sceneProperty.setValue(scene);
        scene.windowProperty().addListener(windowChangeListener);
    }

    @Override
    @CallSuper
    public void onViewDetach(V view, Scene scene) {
        sceneProperty.setValue(null);
        scene.windowProperty().removeListener(windowChangeListener);
    }


    private class WindowChangeListener implements ChangeListener<Window> {
        @Override
        public void changed(ObservableValue<? extends Window> observable, Window oldValue, Window
                newValue) {

            if (oldValue == null && newValue != null) {
                AbstractPresenter.this.onSceneAttach(view, sceneProperty.getValue(), newValue);
            } else if (oldValue != null && newValue == null) {
                AbstractPresenter.this.onSceneDetach(view, sceneProperty.getValue(), oldValue);
            } else if (oldValue != null) {
                AbstractPresenter.this.onSceneDetach(view, sceneProperty.getValue(), oldValue);
                AbstractPresenter.this.onSceneAttach(view, sceneProperty.getValue(), newValue);
            }
        }
    }
}