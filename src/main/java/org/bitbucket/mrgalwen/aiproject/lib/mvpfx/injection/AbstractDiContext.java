

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.injection;

/**
 * @author Michał Kozioł
 * @since 06.10.2016
 */
public abstract class AbstractDiContext implements DiContext {
    @Override
    public void init() {
    }

    @Override
    public void dispose() {
    }
}
