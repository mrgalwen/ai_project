

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.i18n;

import java.util.List;
import java.util.Locale;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;

/**
 * @author Michał Kozioł
 * @since 2016-09-19
 */
public abstract class AbstractMessageProvider implements MessageProvider {
    private List<Locale> supportedLocales;
    private ObjectProperty<Locale> locale = new SimpleObjectProperty<>();

    public AbstractMessageProvider() {
        supportedLocales = FXCollections.observableArrayList(retrieveSupportedLocales());
        locale.addListener(new ChangeListener<Locale>() {
            @Override
            public void changed(ObservableValue<? extends Locale> observable, Locale oldValue,
                                Locale newValue) {
                load();
            }
        });
    }

    @Override
    public List<Locale> getSupportedLocales() {
        if (supportedLocales == null) {
            supportedLocales = retrieveSupportedLocales();
        }

        return supportedLocales;
    }

    @Override
    public final ObjectProperty<Locale> localeProperty() {
        return locale;
    }

    @Override
    public Locale getLocale() {
        return locale.get();
    }

    @Override
    public String getText(String tag, String textId) {
        return getText(tag, textId, null, new Object[0]);
    }

    @Override
    public String getText(String tag, String textId, Object... arguments) {
        return getText(tag, textId, null, arguments);
    }

    @Override
    public String getText(String tag, String textId, String defaultText) {
        return getText(tag, textId, defaultText, new Object[0]);
    }

    protected abstract List<Locale> retrieveSupportedLocales();
}
