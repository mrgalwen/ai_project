

package org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception;

/**
 * @author Michał Kozioł
 * @since 2016-09-29
 */
public class ThemeInfoParsingException extends Exception {
    public ThemeInfoParsingException() {
    }

    public ThemeInfoParsingException(Throwable cause) {
        super(cause);
    }
}
