package org.bitbucket.mrgalwen.aiproject.lib.mvpfx;

import com.sun.javafx.css.StyleManager;

import org.springframework.beans.factory.annotation.Autowired;

import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.Theme;

import java.net.URL;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * @author Michał Kozioł
 * @since 2016-09-27
 */
@SuppressWarnings("unused")
public final class FX {
    private static final SimpleBooleanProperty INITIALIZED = new SimpleBooleanProperty(false);
    private static final SimpleObjectProperty<Image> STAGE_ICON = new SimpleObjectProperty<>();
    private static Stage primaryStage;
    private static boolean devModeEnabled = false;
    private static MvpFxApplication<?> application;

    private FX() {
    }

    public static boolean isDevModeEnabled() {
        return devModeEnabled;
    }

    public static boolean isInitialized() {
        return INITIALIZED.get();
    }

    public static void setInitialized(boolean initialized) {
        FX.INITIALIZED.set(initialized);
    }

    public static SimpleBooleanProperty initializedProperty() {
        return INITIALIZED;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void runAndWait(Runnable runnable) throws Exception {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
        } else {
            FutureTask<?> future = new FutureTask<Void>(runnable, null);
            Platform.runLater(future);
            future.get();
        }
    }

    public static <T> T runAndWait(Callable<T> callable) throws Exception {
        if (Platform.isFxApplicationThread()) {
            return callable.call();
        } else {
            FutureTask<T> future = new FutureTask<>(callable);
            Platform.runLater(future);
            return future.get();
        }
    }

    public static void forceExit() {
        Platform.exit();
        System.exit(0);
    }

    public static void setStageIcon(Image icon) {
        STAGE_ICON.setValue(icon);

        Runnable adder = () -> FX.primaryStage.getIcons().add(icon);

        if (FX.INITIALIZED.get()) {
            adder.run();
        } else {
            FX.INITIALIZED.addListener((observable, oldValue, newValue) -> adder.run());
        }
    }

    public static void applyStageIcon(Stage stage) {
        Runnable adder = () -> stage.getIcons().add(STAGE_ICON.get());

        if (FX.INITIALIZED.get()) {
            adder.run();
        } else {
            FX.INITIALIZED.addListener((observable, oldValue, newValue) -> adder.run());
        }
    }

    public static void installErrorHandler() {
        if (Thread.getDefaultUncaughtExceptionHandler() == null) {
            Thread.setDefaultUncaughtExceptionHandler(new DefaultErrorHandler());
        }
    }

    public static void registerApplication(MvpFxApplication<?> application, Stage primaryStage) {
        FX.installErrorHandler();
        FX.primaryStage = primaryStage;

        primaryStage.sceneProperty().addListener(
                (observable, oldValue, newValue) -> applyStylesheetsTo(newValue));

        FX.application = application;

        if (application.getParameters() != null && application.getParameters()
                .getUnnamed() != null) {
            List<String> unnamed = application.getParameters().getUnnamed();

            if (unnamed.contains("--dev-mode")) {
                devModeEnabled = true;
            }
        }
    }

    public static void applyStylesheetsTo(Scene scene) {
        scene.getStylesheets().addAll(Theme.getStylesheets());

        Theme.getStylesheets().addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(Change<? extends String> it) {
                while (it.next()) {
                    if (it.wasAdded()) {
                        it.getAddedSubList()
                                .forEach((Consumer<String>) s -> scene.getStylesheets().add(s));
                    }

                    if (it.wasRemoved()) {
                        it.getRemoved()
                                .forEach((Consumer<String>) s -> scene.getStylesheets().remove(s));
                    }
                }
            }
        });
    }

    public static void applyStylesheetsTo(Parent parent) {
        parent.getStylesheets().addAll(Theme.getStylesheets());

        Theme.getStylesheets().addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(Change<? extends String> it) {
                while (it.next()) {
                    if (it.wasAdded()) {
                        it.getAddedSubList()
                                .forEach((Consumer<String>) s -> parent.getStylesheets().add(s));
                    }

                    if (it.wasRemoved()) {
                        it.getRemoved()
                                .forEach((Consumer<String>) s -> parent.getStylesheets().remove(s));
                    }
                }
            }
        });
    }

    public static void importStylesheet(String stylesheet) {
        URL css = FX.class.getResource(stylesheet);
        importStylesheet(css);
    }

    public static void importStylesheet(URL stylesheetUrl) {
        Theme.getStylesheets().add(stylesheetUrl.toExternalForm());
    }

    public static void setUserAgentStylesheet(String stylesheet) {
        URL css = FX.class.getResource(stylesheet);

        Application.setUserAgentStylesheet(css.toExternalForm());
        StyleManager.getInstance().addUserAgentStylesheet(css.toExternalForm());

        importStylesheet(css);
    }

    public static FXMLLoader newLoader() {
        FXMLHolder fxmlHolder = application.getContext().newInstance(FXMLHolder.class);
        return fxmlHolder.getFxmlLoader();
    }

    public static Stage newStage() {
        Stage stage = new Stage();

        stage.sceneProperty().addListener(
                (observable, oldValue, newValue) -> applyStylesheetsTo(newValue));

        return stage;
    }

    public static <T> T newInstance(Class<T> clazz) {
        return application.getContext().newInstance(clazz);
    }

    public static class FXMLHolder {
        @Autowired
        private FXMLLoader fxmlLoader;

        public FXMLLoader getFxmlLoader() {
            return fxmlLoader;
        }
    }
}
