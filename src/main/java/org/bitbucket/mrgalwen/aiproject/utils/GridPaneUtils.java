package org.bitbucket.mrgalwen.aiproject.utils;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;

import java.util.Optional;

/**
 * @author Michał Kozioł
 * @since 13.12.2016
 */
public final class GridPaneUtils {
    private GridPaneUtils() {
    }

    public static Optional<Node> getNodeByRowColumnIndex (GridPane gridPane, final int row, final int column) {
        ObservableList<Node> children = gridPane.getChildren();

        for (Node node : children) {
            if(GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
                return Optional.of(node);
            }
        }

        return Optional.empty();
    }
}
