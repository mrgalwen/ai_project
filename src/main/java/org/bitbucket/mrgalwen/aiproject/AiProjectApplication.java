package org.bitbucket.mrgalwen.aiproject;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import jfxtras.resources.JFXtrasFontRoboto;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.mrgalwen.aiproject.features.gameboard.GameBoardView;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.MvpFxApplication;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.Theme;
import org.bitbucket.mrgalwen.aiproject.lib.mvpfx.theme.exception.ThemeLoadingException;

/**
 * @author Michał Kozioł
 */
@Slf4j
public class AiProjectApplication extends MvpFxApplication<GameBoardView> {
    private static AiProjectApplication instance;

    public static AiProjectApplication get() {
        return instance;
    }

    private static final String ICON = "/icon/app.png";
    private static final Image ICON_IMAGE = new Image(
            AiProjectApplication.class.getResource(ICON).toExternalForm());

    public AiProjectApplication() {
        super(ICON_IMAGE);
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initMvpFx() {
        JFXtrasFontRoboto.loadAll();
    }

    @Override
    protected void startMvpFx(Stage stage) {
        instance = this;
        stage.setOnCloseRequest(event -> Platform.exit());
        log.info("{}", getParameters().toString());

        try {
            Theme.loadFromAssets("dark");
        } catch (ThemeLoadingException e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void stop() throws Exception {
        System.exit(0);
    }
}