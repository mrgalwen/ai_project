package org.bitbucket.mrgalwen.aiproject;

import javafx.application.Application;

/**
 * @author Michał Kozioł
 */
public class Launcher {
    public static void main(String[] args) {
        Application.launch(AiProjectApplication.class, args);
    }
}
