var gulp    = require('gulp');
var less    = require('gulp-less');
var rename  = require('gulp-rename');
var plumber = require('gulp-plumber');
var gutil   = require('gulp-util');
var chalk   = require('chalk');

/**
 * HELPERS
 */

/**
 * @param err {Error}
 */
function defaultErrorHandler(err) {
    gutil.log(chalk.bgRed(err.message));
    this.emit('end');
}

function compileLess(theme) {
    return gulp.src(`./less/theme/${theme}.less`)
        .pipe(plumber())
        .pipe(rename(function(path) {
            path.basename = 'style';
        }))
        .pipe(less())
        .on('error', defaultErrorHandler)
        .pipe(gulp.dest(`./assets/theme/${theme}`));
}

/**
 * TASKS
 */

gulp.task('less-compile-light', function () {
    return compileLess('light');
});

gulp.task('less-compile-dark', function () {
    return compileLess('dark');
});

gulp.task('less-watch', function () {
    gulp.watch('./less/**/*.less', ['less-compile-dark', 'less-compile-light']);
})

gulp.task('default', ['less-compile-light', 'less-compile-dark', 'less-watch']);
gulp.task('compile', ['less-compile-light', 'less-compile-dark']);